import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { IsLoggedInGuard } from './guards/is-logged-in.guard';
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';
import { MainLayoutComponent } from './shared/layouts/main-layout/main-layout.component';
import { NotFoundComponent } from './shared/layouts/not-found/not-found.component';
import { NoAccessComponent } from './shared/layouts/no-access/no-access.component';
import { ChangePasswordComponent } from './pages/auth/change-password/change-password.component';

const routes: Routes = [

  { path: 'auth', canActivate: [IsLoggedInGuard], component: AuthLayoutComponent, loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule) },
  { path: 'connect', canActivate: [AuthGuard], component: MainLayoutComponent, loadChildren: () => import('./pages/main-layout/main-layout.module').then(m => m.MainLayoutModule) },
  { path: '', pathMatch: 'full', redirectTo: 'auth' },
  { path: 'change-password', component: ChangePasswordComponent },
  { path: 'non-authentic', component: NoAccessComponent },
  { path: '**', component: NotFoundComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
