import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './pages/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  isCollapsed = false;

  constructor(private authService: AuthService, private router: Router){}

  ngOnInit() {
    // if(!this.authService.getToken()){
    //   this.router.navigate(['connect/sessions'])
    // }
    // else{
    //   this.router.navigate(['connect/user-account'])
    // }
  }

}
