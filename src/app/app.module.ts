import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { IconsProviderModule } from './icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { MainLayoutComponent } from './shared/layouts/main-layout/main-layout.component';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { UserSidebarComponent } from './shared/layouts/user-sidebar/user-sidebar.component';
import { AuthGuard } from './guards/auth.guard'
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';
import { AppHelper } from './shared/utils/helper';
import { NzMessageModule } from 'ng-zorro-antd/message';

@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    UserSidebarComponent,
    AuthLayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule,
    NzLayoutModule,
    NzRadioModule,
    NzMessageModule
  ],
  exports: [
    MainLayoutComponent,
    NzLayoutModule
  ],
  providers: [AuthGuard, AppHelper],
  bootstrap: [AppComponent]
})
export class AppModule { }
