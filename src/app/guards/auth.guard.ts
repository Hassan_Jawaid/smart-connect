import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../pages/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private auth: AuthService){}

  canActivate(): boolean {
    if(this.auth.isAuthenticated()){
      // this.router.navigate(['**'])
      return true
    }
    else{
      this.router.navigate(['auth/login'])
      return false;
    }
  }
  
}
