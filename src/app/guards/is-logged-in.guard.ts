import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../pages/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class IsLoggedInGuard implements CanActivate {

  constructor(private router: Router, private auth: AuthService){}

  canActivate(): boolean {
    if(this.auth.isAuthenticated()){
      this.router.navigate(['connect/contacts'])
      console.log('non authentic')
      return false;
      
    }
    else{
      // this.router.navigate(['auth/login'])
      console.log('login')
      return true
    }
  }
  
}
