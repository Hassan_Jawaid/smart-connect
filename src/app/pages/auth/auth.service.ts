import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/shared/api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private api: ApiService, private router: Router) { }

  setToken(user){
    console.log('setting token', user)
    localStorage.setItem('access_token', JSON.stringify(user.accessToken))
    localStorage.setItem('user_info', JSON.stringify(user))
  }

  getToken(){
    return JSON.parse(localStorage.getItem('user_info'))
  }

  registerUser(params) {
    return this.api.post('/user/registerUser', params)
  }

  loginUser(params) {
    return this.api.post('/user/loginUser', params)
  }

  resendVerification(params, userId) {
    let headers = {access_token: this.getToken().accessToken, logged_in_user_id: userId }
    return this.api.post('/user/resendUserVerificationCode', {}, params, headers)
  }

  verifyUser(params) {
    let headers = {access_token: this.getToken().accessToken}
    return this.api.post('/user/verifyUserVerificationCode', {}, params, headers)
  }

  forgotPassword(params) {
    return this.api.put('/user/forgotPassword', null, params)
  }

  isAuthenticated() {
    return localStorage.getItem('access_token')
  }

  updatePassword(params, userId){
    let headers:any = {access_token: this.getToken().accessToken, logged_in_user_id: userId}
    return this.api.put('/user/updatePassword', null, params, headers )
  }
}
