import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppHelper } from 'src/app/shared/utils/helper';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.less']
})
export class ChangePasswordComponent implements OnInit {

  constructor(private message: NzMessageService, private helper: AppHelper, private authService: AuthService, private router: Router) { }
  resetPassForm = new FormGroup({
    user_old_password: new FormControl(null, Validators.required),
    user_new_password: new FormControl(null, [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/)]),
    RePassword: new FormControl(null, Validators.required)
  });
  isSubmitted = false;
  showRePass = false;
  showPass = false;
  loading = false;
  userInfo = this.authService.getToken();

  ngOnInit(): void {
  }

  switchRePassType() {
    this.showRePass = !this.showRePass;
  }

  switchPassType() {
    this.showPass = !this.showPass;
  }

  submit() {
    // console.log(this.resetPassForm.controls['RePassword'].value != this.resetPassForm.controls['Password'].value)
    this.isSubmitted = true;
    if (this.resetPassForm.valid && (this.resetPassForm.controls['user_old_password'].value && this.resetPassForm.controls['RePassword'].value == this.resetPassForm.controls['user_new_password'].value)) {
      this.loading = true;
      let requestParams = this.resetPassForm.getRawValue()
      requestParams.user_id = this.userInfo.userId;
      const params = this.helper.prepareRequestParams(requestParams);
      this.authService.updatePassword(params, this.userInfo.userId)
      .subscribe(val =>{
        this.loading = false;
        this.message.create('success', `Password changed successfully!`);
        this.router.navigate(['connect/contacts'])
      },err=>{
        this.loading = false;
      })
    }
  }


}
