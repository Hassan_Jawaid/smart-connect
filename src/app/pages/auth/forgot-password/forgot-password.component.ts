import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppHelper } from 'src/app/shared/utils/helper';
import { AppConfig } from 'src/app/utilities/config';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.less']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private router: Router, private auth: AuthService, private helper: AppHelper, private message: NzMessageService) { }
  forgotPassForm = new FormGroup({
    Email: new FormControl(null, [Validators.required, Validators.pattern(AppConfig.regRexPatterns.email)])
  });
  buttonLoading = false;
  buttonStateClass = '';
  isSubmitted = false;
  showMsg = false;
  loading = false;

  ngOnInit(): void {
  }

  handleOk(){
    this.showMsg = false;
  }

  submit() {
    this.isSubmitted = true;

    if (this.forgotPassForm.valid) {
      this.loading = true;
      const requestParams = {
        email: this.forgotPassForm.getRawValue().Email
      }
      const params = this.helper.prepareRequestParams(requestParams)
       this.auth.forgotPassword(params)
      .subscribe(val =>{
        console.log(val)
        this.loading = false;
        this.showMsg = true;
        setTimeout(() => {
          localStorage.setItem('resetPassword', JSON.stringify({reset: true}))
          this.router.navigate(['auth/login']) 
        }, 2000);
      },err=>{   
        console.log(err)
        this.loading = false;
        this.message.create('error', err.error?.failure ?? err.error?.info ?? 'Failed!');
      }); 
    }
  }

}
