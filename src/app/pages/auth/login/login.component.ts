import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AuthService } from '../auth.service';
import { HttpParams } from '@angular/common/http';
import { AppConfig } from 'src/app/utilities/config';
import { AppHelper } from 'src/app/shared/utils/helper';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  showPass = false;
  loginForm = new FormGroup({
    Email: new FormControl(null, [Validators.required, Validators.pattern(AppConfig.regRexPatterns.email)]),
    Password: new FormControl(null, [Validators.required]),
  });
  buttonLoading = false;
  buttonStateClass = '';
  isSubmitted = false;
  loading = false;

  constructor(private router: Router, private authService: AuthService, private helper: AppHelper, private message: NzMessageService) { }

  ngOnInit() {
  }

  switchPassType() {
    this.showPass = !this.showPass;
  }

  loginSubmit() {
    this.isSubmitted = true;
    if (this.loginForm.valid) {
      this.loading = true;
      let body = this.loginForm.getRawValue();

      this.buttonLoading = true;
      this.buttonStateClass = 'loading';
      const requestParams = {
        email: body.Email,
        password: body.Password
      }
      const params = this.helper.prepareRequestParams(requestParams)
      this.authService.loginUser(params)
      .subscribe(val =>{
        console.log(val)
        this.loading = false;
        if(localStorage.getItem('resetPassword')){
          if(val['failure'] || val['info']){
            this.message.create('error', val['failure'] ? val['failure'] : val['info']);
          }
          if(val['accessToken']){
            this.authService.setToken(val);
            this.router.navigate(['auth/change-password']) 
          }
        }
        else{
          if(val['isVerified']){
        if(val['isVerified'] != 'false'){
          this.message.create('success', `User successfully logged in!`);
          this.authService.setToken(val);
          this.router.navigate(['connect/contacts']) 
        }
        else{
          let user = {
            userId: val['userId'],
            accessToken: val['accessToken']
          }
          this.authService.setToken(user)
          this.message.create('info', `You are not verified yet, please verify your account!`);
          this.router.navigate(['auth/verify']);
        }
      }
      if(val['failure'] || val['info']){
        this.message.create('error', val['failure'] ? val['failure'] : val['info']);
      }
      
      }
      },err=>{   
        console.log(err)
        this.loading = false;
        this.message.create('error', err.error?.failure ?? err.error?.info ?? 'Login failed!');
      });
        
    }
  }

}
