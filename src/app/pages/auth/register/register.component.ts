import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// import { DataServiceService } from '../../../data-service.service';
import { LoginModel } from '../../../shared/models/user';
import { AuthService } from '../auth.service'; 
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpParams } from '@angular/common/http';
import { AppConfig } from '../../../utilities/config'
import { AppHelper } from '../../../shared/utils/helper'
import { SharedService } from 'src/app/shared/shared.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {
  showPass = false;
  registerForm: FormGroup;
  buttonLoading = false;
  buttonStateClass = '';
  isSubmitted = false;
  radioValue;
  loading = false;
  showVerification = false;
  countriesList: any = [];
  reCaptchaSuccess = false;

  constructor(private sharedService: SharedService, private authService: AuthService, private router: Router, private helper: AppHelper, private message: NzMessageService) { 
    this.registerForm = new FormGroup({
      Name: new FormControl(null, Validators.required),
      LastName: new FormControl(null, Validators.required),
      Email: new FormControl(null, [Validators.required, Validators.pattern(AppConfig.regRexPatterns.email)]),
      ConfirmEmail: new FormControl(null, [Validators.required, Validators.pattern(AppConfig.regRexPatterns.email)]),
      Password: new FormControl(null, [Validators.required, Validators.pattern(AppConfig.regRexPatterns.password)]),
      ConfirmPassword: new FormControl(null, [Validators.required, this.confirmValidator]),
      IsPublic: new FormControl(false),
      CanSuggest: new FormControl(false),
      Birthday: new FormControl(null, Validators.required),
      UserType: new FormControl('1', Validators.required),
      Gender: new FormControl(null, Validators.required),
      Mobile: new FormControl(null, Validators.required),
      Country: new FormControl(null, Validators.required),
    });
  }

  ngOnInit() {
    let that = this;
    window['grecaptcha'].render('smrt-google-recaptcha', {
      'sitekey' : '6LfSILQbAAAAANt42jIvLIClkNEMtKfyvSnEJYTf',
      'callback': function() {
        that.reCaptchaSuccess = true;
      }
    });

    this.sharedService.getAllCountries().subscribe(res => {
      this.countriesList = res;
    })
  }

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.registerForm.controls.Password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  switchPassType() {
    this.showPass = !this.showPass;
  }

  async register() {
    this.isSubmitted = true;
    if (this.registerForm.valid && this.reCaptchaSuccess) {
      this.loading = true;
      let body = this.registerForm.getRawValue();
      let requestParams = {
        full_name: body.Name,
        last_name: body.LastName,
        email: body.Email,
        password: body.Password,
        is_public: body.IsPublic,
        send_contact_suggestions: body.CanSuggest,
        user_type_id: body.UserType,
        country: body.Country,
        mobile: body.Mobile,
        birthday: body.Birthday,
        gender: body.Gender
      }
      this.buttonLoading = true;
      this.buttonStateClass = 'loading';

      let params = this.helper.prepareRequestParams(requestParams)
      // console.log(params)
      this.authService.registerUser(params)
      .subscribe(val =>{
        console.log(val);
        this.loading = false;
        this.showVerification = true;
        this.message.create('success', `User successfully registered!`);
        let user = {
          userId: val['userId'],
          accessToken: val['accessToken']
        }
        this.authService.setToken(user)
        this.router.navigate(['auth/verify'])
      },err=>{   
        console.log(err)
        this.loading = false;
        this.message.create('error', err.error?.failure ?? 'Registration API failed!');
      });
    }
  }
  
  onChange(result: Date): void {
    console.log('onChange: ', result);
  }

  resendVerification() {
    this.message.create('success', `Verification link to your email has been sent!`);
    this.router.navigate(['connect/contacts']) 
  }

}
