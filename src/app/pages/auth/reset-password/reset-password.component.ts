import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppConfig } from 'src/app/utilities/config';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.less']
})
export class ResetPasswordComponent implements OnInit {

  constructor(private router: Router, private message: NzMessageService) { }
  resetPassForm = new FormGroup({
    // PreviousPassword: new FormControl(null, Validators.required),
    Password: new FormControl(null, [Validators.required, Validators.pattern(AppConfig.regRexPatterns.password)]),
    RePassword: new FormControl(null, Validators.required)
  });
  isSubmitted = false;
  showRePass = false;
  showPass = false;
  loading = false;

  ngOnInit(): void {
  }

  switchRePassType() {
    this.showRePass = !this.showRePass;
  }

  switchPassType() {
    this.showPass = !this.showPass;
  }

  submit() {
    // console.log(this.resetPassForm.controls['RePassword'].value != this.resetPassForm.controls['Password'].value)
    this.isSubmitted = true;
    if (this.resetPassForm.valid && (this.resetPassForm.controls['RePassword'].value == this.resetPassForm.controls['Password'].value)) {
      this.loading = true;
      setTimeout(() => {
        this.loading = false;
        this.message.create('success', `Password reset successfully!`);
        this.router.navigate(['auth/login'])
      }, 1000);
    }
  }



}
