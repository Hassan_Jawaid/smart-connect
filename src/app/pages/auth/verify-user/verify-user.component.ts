import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginModel } from '../../../shared/models/user';
import { AuthService } from '../auth.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-verify-user',
  templateUrl: './verify-user.component.html',
  styleUrls: ['./verify-user.component.less']
})
export class VerifyUserComponent implements OnInit {
  showPass = false;
  verifyForm: FormGroup;
  buttonLoading = false;
  buttonStateClass = '';
  isSubmitted = false;
  radioValue;
  loading = false;
  userInfo = this.authService.getToken();

  constructor(private authService: AuthService, private router: Router, private message: NzMessageService) {
    this.verifyForm = new FormGroup({
      otpFields: new FormArray([
        new FormGroup({
          OTPCodePart: new FormControl(null, [Validators.maxLength(1), Validators.minLength(1)]),
        }),
        new FormGroup({
          OTPCodePart: new FormControl(null, [Validators.maxLength(1), Validators.minLength(1)]),
        }),
        new FormGroup({
          OTPCodePart: new FormControl(null, [Validators.maxLength(1), Validators.minLength(1)]),
        }),
        new FormGroup({
          OTPCodePart: new FormControl(null, [Validators.maxLength(1), Validators.minLength(1)]),
        }),
      ]),
    });
  }

  ngOnInit() {
  }

  onFocus(e) {
    e.target.select();
  }

  onKeyDown(e) {
    var key = e.which;

    if (key === 9 || (key >= 48 && key <= 57)) {
      return true;
    }

    e.preventDefault();
    return false;
  }

  goToNextInput(e) {
    var key = e.which,
      sib = e.target.parentElement.nextElementSibling ? e.target.parentElement.nextElementSibling.children[0] : null; // gets next element

    if (key != 9 && (key < 48 || key > 57)) {
      e.preventDefault();
      return false;
    }

    if (key === 9) {
      return true;
    }

    if (!sib) {
      sib = e.target.parentElement.parentElement.children[0].children[0]; // gets first element to make an infinite tabbing
    }
    sib.select()
    sib.focus();
  }

  resendOTP() {
    const params = new HttpParams()
    .set('user_id', this.userInfo.userId);
    this.authService.resendVerification(params, this.userInfo.userId).subscribe(val => {
      this.message.create('success', `Email resent successfully`);
    }, err => {
      this.message.create('error', "Resend Email couldn't be send");
    });
  }

  async loginSubmit() {
    this.isSubmitted = true;
    if (this.verifyForm.valid) {
      this.loading = true;
      let body = this.verifyForm.getRawValue();
      let otpCode = body.otpFields.map(x => x.OTPCodePart);

      this.buttonLoading = true;
      this.buttonStateClass = 'loading';

      const params = new HttpParams()
        .set('user_id', this.userInfo.userId)
        .set('code', otpCode.join(''))
        .set('access_token', this.userInfo.accessToken);
      console.log('verifying')
      this.authService.verifyUser(params)
        .subscribe(val => {
          this.loading = false;
          if(val['failure']){
            this.message.create('error', val['failure'])
          }
          else{
            this.message.create('success', `User verified successfully!`);
            this.router.navigate(['auth/login'])
          }
         
        }, err => {
          console.log(err)
          this.loading = false;
          this.message.create('error', err.error.failure ?? `User couldn't be verified!`);
        });
    }
  }

}
