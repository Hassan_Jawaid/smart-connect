import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-archives',
  templateUrl: './archives.component.html',
  styleUrls: ['./archives.component.less'],
})
export class ArchivesComponent implements OnInit {
  constructor(private message: NzMessageService) {}

  split = false;
  selectedContacts = [];
  individualImg;
  businessImg;
  letters = [
    '#',
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z',
  ];
  showDeleteMsg = false;
  showArchiveMsg = false;
  showStarMsg = false;
  showRoleMsg = false;
  searchValue;
  role = '1';
  allow = false;
  grantRoles = [];
  nodes = [
    {
      title: 'Smart',
      key: '100',
      expanded: false,
      children: [
        {
          title: 'Connect',
          key: '1001',
          expanded: false,
          children: [
            {
              title: 'Details',
              key: '10010',
              children: [
                {
                  title: 'Name',
                  key: '100101',
                },
                {
                  title: 'Last Name',
                  key: '100102',
                },
              ],
            },
          ],
        },
        {
          title: 'Pointer',
          key: '1002',
        },
        {
          title: 'Signup',
          key: '1002',
        },
      ],
      
    },
  ];
  allNodes = this.nodes;

  searchedNodes = [];

  roles = [
    'Administration',
    'Administration 2',
    'Receivable Accounts',
    'Payable Accounts',
  ];

  dataSet = [
    
  ];

  sorted;
  grouped;
  result;
  profileType = '1';
  smrtTab = 'privacy';

  ngOnInit(): void {
    this.sortData(this.dataSet);
    this.sortByThis('firstName');
  }

  sortData(data) {
    this.sorted = data.sort((a, b) => (a.firstName > b.firstName ? 1 : -1));
  }

  editContact(id){
    this.split = true;
    console.log('editt')
  }

  selectContact(id = null) {
    console.log(id);
    let selectedContact = this.dataSet.find((d) => d.id == id);
    if (selectedContact) {
      selectedContact.selected = !selectedContact.selected;
    }
    let ind = this.selectedContacts.findIndex((contact) => contact.id == id);
    ind >= 0
      ? this.selectedContacts.splice(ind, 1)
      : this.selectedContacts.push(selectedContact);
  }

  checkLetter(letter) {
    return document.getElementById(letter);
  }

  splitScreen() {
    this.split = true;
  }

  sortByThis(type) {
    let tempGrouped = this.sorted.reduce((groups, contact) => {
      const letter = contact[type].charAt(0);

      groups[letter] = groups[letter] || [];
      groups[letter].push(contact);

      return groups;
    }, {});

    this.result = Object.keys(tempGrouped).map((key) => ({
      key,
      contacts: tempGrouped[key],
    }));
    let elems: any = document
      .getElementById('parent-ul')
      ?.getElementsByTagName('li');
    if (elems && elems.length > 0) {
      for (let e of elems) {
        e.classList.remove('active');
      }
    }
    document.getElementById(type)?.classList.add('active');
  }

  noSplit() {
    this.split = false;
  }

  showModal(type) {
    this[type] = !this[type];
  }

  ngOnDestroy() {
    console.log('destroyeddd');
  }

  showNotification() {
    this.message.create('info', `No action defined to this button yet!`);
  }

  searchContact(e) {
    console.log('aaaabbbbbbbb');
    let value = e;
    let filter = this.dataSet.filter((data) => data.firstName.includes(value));
    // console.log(e, this.dataSet, filter, this.dataSet.filter(data=>data.firstName.toLowerCase().includes(value)))
    if (
      this.dataSet.filter(
        (data) =>
          data.lastName.toLowerCase().includes(value) ||
          data.firstName.toLowerCase().includes(value)
      ).length >= 0
    ) {
      this.sortData(
        this.dataSet.filter(
          (data) =>
            data.lastName.toLowerCase().includes(value) ||
            data.firstName.toLowerCase().includes(value)
        )
      );
      this.sortByThis('firstName');
    }
  }

  scrollToThis(val) {
    console.log(val);
    console.log(document.getElementById('letter-' + val));
    var objDiv = document.getElementById('letter-' + val);
    objDiv.scrollIntoView({ behavior: 'smooth' });
  }

  allowChanged() {
    console.log('heyy');
    this.allow = !this.allow;
    if(!this.allow){
      this.showRoleMsg = true;
    }
    console.log(this.allow);
  }

  searchPermission(e){
    console.log(e);
    const children = this.nodes[0].children;
    if(e.target.value == ''){
      console.log('here')
      this.searchedNodes = []
      console.log(children)
      this.nodes = [...this.allNodes]
    }
    else{
      let value = e.target.value;
      this.searchedNodes = [...this.nodes];
      this.searchedNodes[0].children = [...this.searchedNodes[0].children.filter(child=>child.title.toLowerCase().includes(value))]
      // console.log(this.nodes,this.searchedNodes)
    }
  }

  pictureChanged(e){
    if(this.profileType == '1'){
      document.getElementById('indImg').setAttribute('src', URL.createObjectURL(e.target.files[0]))
    }
    else{
      document.getElementById('busImg').setAttribute('src', URL.createObjectURL(e.target.files[0]))

    }
  }
}
