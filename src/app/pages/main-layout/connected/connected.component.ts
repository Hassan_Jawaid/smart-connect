import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-connected',
  templateUrl: './connected.component.html',
  styleUrls: ['./connected.component.less'],
})
export class ContactsConnectedComponent implements OnInit {
  constructor(private message: NzMessageService) {}

  split = false;
  attachments = []
  selectedContacts = [];
  letters = [
    '#',
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z',
  ];
  showDeleteMsg = false;
  showArchiveMsg = false;
  showStarMsg = false;
  showRoleMsg = false;
  searchValue;
  role = '1';
  allow = false;
  grantRoles = [];
  nodes = [
    {
      title: 'Smart',
      key: '100',
      expanded: false,
      children: [
        {
          title: 'Connect',
          key: '1001',
          expanded: false,
          children: [
            {
              title: 'Details',
              key: '10010',
              children: [
                {
                  title: 'Name',
                  key: '100101',
                },
                {
                  title: 'Last Name',
                  key: '100102',
                },
              ],
            },
          ],
        },
        {
          title: 'Pointer',
          key: '1002',
        },
        {
          title: 'Signup',
          key: '1002',
        },
      ],
      
    },
  ];
  allNodes = this.nodes;

  searchedNodes = [];

  roles = [
    'Administration',
    'Administration 2',
    'Receivable Accounts',
    'Payable Accounts',
  ];

  dataSet = [
    {
      firstName: 'Andrew',
      lastName: 'Raymond',
      email: 'abdul@test.com',
      country: 'Italy',
      phone: '03213124431',
      selected: false,
      id: 9,
      picture:
        'https://www.microsoft.com/en-us/research/wp-content/uploads/2017/09/avatar_user_36443_1506533427.jpg',
    },
    {
      firstName: 'Ashley',
      lastName: 'Carl',
      email: 'ali@test.com',
      phone: '03213124431',
      country: 'Spain',
      selected: false,
      id: 1,
      picture:
        'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80',
    },

    {
      firstName: 'Brandon',
      lastName: 'Jones',
      email: 'bilal@test.com',
      phone: '03213124431',
      country: 'Russia',
      selected: false,
      id: 2,
      picture: 'https://portotheme.com/html/porto/7.5.0/img/avatars/avatar.jpg',
    },

    {
      firstName: 'Charlie',
      lastName: 'Young',
      email: 'chaudhry@test.com',
      phone: '03213124431',
      country: 'Germany',
      selected: false,
      id: 4,
      picture:
        'https://images-na.ssl-images-amazon.com/images/I/81K7JEQaXjL._SY600_.jpg',
    },
    {
      firstName: 'Danny',
      lastName: 'Hart',
      email: 'dani@test.com',
      phone: '03213124431',
      country: 'Australia',
      selected: false,
      id: 5,
      picture:
        'http://demos.thementic.com/wordpress/WC01/WC010007/wp-content/uploads/2019/02/t3.jpg',
    },
    {
      firstName: 'Douglas',
      lastName: 'Costa',
      email: 'fhahroz@test.com',
      phone: '03213124431',
      country: 'England',
      selected: false,
      id: 8,
      picture:
        'https://amzsummits.com/wp-content/uploads/2019/05/Ferry-Vermeulen.jpeg',
    },
    {
      firstName: 'George',
      lastName: 'Sandow',
      email: 'hamza@test.com',
      phone: '03213124431',
      country: 'Zimbabwe',
      selected: false,
      id: 10,
      picture:
        'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80',
    },
    {
      firstName: 'Tom',
      lastName: 'Garay',
      email: 'sherry@test.com',
      phone: '03213124431',
      country: 'South Africa',
      selected: false,
      id: 6,
      picture: 'https://cxl.com/wp-content/uploads/2016/03/nate_munger.png',
    },
  ];

  sorted;
  grouped;
  result;
  profileType = '1';
  smrtTab = 'privacy';

  ngOnInit(): void {
    this.sortData(this.dataSet);
    this.sortByThis('firstName');
    // setTimeout(() => {
    //   this.grantRoles = [{
    //     type: 'Role',
    //     role: 'Sale',
    //     description: 'test'
    //   },{
    //     type: 'Role',
    //     role: 'Procurement',
    //     description: 'test'
    //   },{
    //     type: 'Permission',
    //     role: 'View Invoice',
    //     description: 'test'
    //   },{
    //     type: 'Role',
    //     role: 'Attach',
    //     description: 'test'
    //   }]
    // }, 5000);
  }

  sortData(data) {
    this.sorted = data.sort((a, b) => (a.firstName > b.firstName ? 1 : -1));
  }

  editContact(id){
    this.split = true;
    console.log('editt')
  }

  selectContact(id = null) {
    console.log(id);
    let selectedContact = this.dataSet.find((d) => d.id == id);
    if (selectedContact) {
      selectedContact.selected = !selectedContact.selected;
    }
    let ind = this.selectedContacts.findIndex((contact) => contact.id == id);
    ind >= 0
      ? this.selectedContacts.splice(ind, 1)
      : this.selectedContacts.push(selectedContact);
  }

  checkLetter(letter) {
    return document.getElementById(letter);
  }

  splitScreen() {
    this.split = true;
  }

  sortByThis(type) {
    let tempGrouped = this.sorted.reduce((groups, contact) => {
      const letter = contact[type].charAt(0);

      groups[letter] = groups[letter] || [];
      groups[letter].push(contact);

      return groups;
    }, {});

    this.result = Object.keys(tempGrouped).map((key) => ({
      key,
      contacts: tempGrouped[key],
    }));
    let elems: any = document
      .getElementById('parent-ul')
      ?.getElementsByTagName('li');
    if (elems && elems.length > 0) {
      for (let e of elems) {
        e.classList.remove('active');
      }
    }
    document.getElementById(type)?.classList.add('active');
  }

  noSplit() {
    this.split = false;
  }

  showModal(type) {
    this[type] = !this[type];
  }

  ngOnDestroy() {
    console.log('destroyeddd');
  }

  showNotification() {
    this.message.create('info', `No action defined to this button yet!`);
  }

  searchContact(e) {
    console.log('aaaabbbbbbbb');
    let value = e;
    let filter = this.dataSet.filter((data) => data.firstName.includes(value));
    // console.log(e, this.dataSet, filter, this.dataSet.filter(data=>data.firstName.toLowerCase().includes(value)))
    if (
      this.dataSet.filter(
        (data) =>
          data.lastName.toLowerCase().includes(value) ||
          data.firstName.toLowerCase().includes(value)
      ).length >= 0
    ) {
      this.sortData(
        this.dataSet.filter(
          (data) =>
            data.lastName.toLowerCase().includes(value) ||
            data.firstName.toLowerCase().includes(value)
        )
      );
      this.sortByThis('firstName');
    }
  }

  scrollToThis(val) {
    console.log(val);
    console.log(document.getElementById('letter-' + val));
    var objDiv = document.getElementById('letter-' + val);
    objDiv.scrollIntoView({ behavior: 'smooth' });
  }

  allowChanged() {
    console.log('heyy');
    this.allow = !this.allow;
    if(!this.allow){
      this.showRoleMsg = true;
    }
    console.log(this.allow);
  }

  searchPermission(e){
    console.log(e);
    const children = this.nodes[0].children;
    if(e.target.value == ''){
      console.log('here')
      this.searchedNodes = []
      console.log(children)
      this.nodes = [...this.allNodes]
    }
    else{
      let value = e.target.value;
      this.searchedNodes = [...this.nodes];
      this.searchedNodes[0].children = [...this.searchedNodes[0].children.filter(child=>child.title.toLowerCase().includes(value))]
      // console.log(this.nodes,this.searchedNodes)
    }
  }

  addAttachment(e){
    document.getElementById('attachment').click()
  }

  attachmentSelected(e){
    this.attachments.push({img: e.target.files[0]})
    let id = this.attachments.length -1;
    setTimeout(() => {
      document.getElementById('img-'+id).setAttribute('src',URL.createObjectURL(e.target.files[0]))
    },500);
  }

  pictureChanged(e){
    if(this.profileType == '1'){
      document.getElementById('indImg').setAttribute('src', URL.createObjectURL(e.target.files[0]))
    }
    else{
      document.getElementById('busImg').setAttribute('src', URL.createObjectURL(e.target.files[0]))

    }
  }
}
