import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { BehaviorSubject } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { SharedService } from 'src/app/shared/shared.service';
import { AppHelper } from 'src/app/shared/utils/helper';
import { ContactsService } from '../../../services/contacts.service'
import { AuthService } from '../../auth/auth.service';
import { dataSet } from './contacts-data.mock';
import { nodesData } from './nodes-data.mock';
@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.less'],
})
export class ContactsComponent implements OnInit {
  constructor(private message: NzMessageService, private router: Router, private userService: UserService, private sharedService: SharedService, private contactService: ContactsService, private auth: AuthService, private helper: AppHelper) { }
  contactForm = new FormGroup({
    userType: new FormControl('1', Validators.required),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    birthday: new FormControl(null),
    gender: new FormControl(''),
    country: new FormControl(''),
    state: new FormControl(''),
    city: new FormControl(''),
    postalCode: new FormControl(''),
    userAddress: new FormControl(''),
    profession: new FormControl(''),
    occupation: new FormControl(''),
    company: new FormControl(''),
    position: new FormControl(''),
    website: new FormControl(''),
    registrationEmail: new FormControl(''),
    notes: new FormControl(''),
    profilePicture: new FormControl(''),
  })
  originals:any = [];
  selectedContactToPerformAction;
  selectedContact;
  updatingContact = false;
  attachmentsUploaded = new BehaviorSubject(false)
  userInfo = this.auth.getToken();
  socials = [
    { type: 1, name: 'Facebook', img: 'https://raw.githubusercontent.com/edent/SuperTinyIcons/master/images/svg/facebook.svg' },
    { type: 2, name: 'Instagram', img: 'https://raw.githubusercontent.com/edent/SuperTinyIcons/master/images/svg/instagram.svg' },
    { type: 3, name: 'Snapchat', img: 'https://raw.githubusercontent.com/edent/SuperTinyIcons/master/images/svg/snapchat.svg' },
    { type: 4, name: 'Twitter', img: 'https://raw.githubusercontent.com/edent/SuperTinyIcons/master/images/svg/twitter.svg' }
  ]
  socialMedias: any = [];
  codesList: any = []
  phoneNumbers: any = [];
  emails: any = [];
  split = false;
  splitView = false;
  selectedContacts = [];
  addingContact = false;
  attachments = []
  smrtTab2 = 'mine';
  image;
  letters = ['#', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  // bad logic, but using for quick
  lettersCount = {};
  showDeleteMsg = false;
  showArchiveMsg = false;
  showStarMsg = false;
  showRoleMsg = false;
  searchValue;
  following = []
  role = '1';
  allow = false;
  grantRoles = [];
  nodes = nodesData;
  allNodes = this.nodes;

  searchedNodes = [];

  roles = ['Administration', 'Administration 2', 'Receivable Accounts', 'Payable Accounts'];

  dataSet = dataSet;
  uploadedAttachments = [];
  sorted;
  grouped;
  result = [];
  profileType = '1';
  smrtTab = 'privacy';
  showRoleTemplate = false;
  countriesList: any = []
  contacts:any = []
  ngOnInit(): void {
    this.getAllContacts()
    this.sharedService.getAllCountries().subscribe(res => {
      this.countriesList = res;
    });
    this.sharedService.getAllCodes().subscribe(res => {
      this.codesList = res;
      this.codesList.map(c => {
        c.callingCode = c.callingCodes[0];
        delete c.callingCodes;
      })
    })
    let user = this.auth.getToken();
    const params = new HttpParams()
      .set('id', user?.userId)
      .set('access_token', user?.accessToken)
    

    this.helper.initSticky('.js-sticky-index', 'fixed--state');
  }

  sortData(data) {
    this.sorted = data.sort((a, b) => (a.firstName > b.firstName ? 1 : -1));
  }

  editContact(id) {
    this.updatingContact = true;
    this.split = true;
    this.splitView = false;
    console.log('editt')
    Object.keys(this.contactForm.controls).map((key, ind) => {
      this.contactForm.controls[key].enable()
    })
    setTimeout(() => {
      this.helper.resetFixedState('.js-sticky-index', 'fixed--state');
    }, 50)
  }

  viewContact(id) {
    this.splitView = true;
    this.split = false;
    setTimeout(() => {
      this.helper.resetFixedState('.js-sticky-index', 'fixed--state');
    }, 50)
  }

  addContact() {
    document.querySelector('.js-spinner-overlay')['style'].display = "flex";
    let phones = [...this.phoneNumbers];
    phones.forEach(ph => {
      delete ph.flag;
    });
    let socialProfiles = [...this.socialMedias];
    socialProfiles.forEach(s => {
      delete s.img;
    });
        let payload = {
          ...this.contacts.find(c=>c.id == this.selectedContact),
          userTypeId: parseInt(this.profileType),
          birthday: this.contactForm.get('birthday').value,
          email: this.emails.length ? this.emails[0] : '',
          firstName: this.contactForm.get('firstName').value,
          lastName: this.contactForm.get('lastName').value,
          profilePicture: this.contactForm.get('profilePicture')?.value ? this.contactForm.get('profilePicture').value : '',
          contactAttachments: this.uploadedAttachments,
          contactEmails: this.fillEmails(),
          contactHasSyncedContacts: "false",
          contactPhoneNumbers: phones,
          contactOccupation: {
            companyName: this.contactForm.get('company').value,
            position: this.contactForm.get('position').value,
            occupation: this.contactForm.get('occupation').value,
            profession: this.contactForm.get('profession').value
          },
          contactAddress: {
            street: this.contactForm.get('userAddress').value,
            city: this.contactForm.get('city').value,
            state: this.contactForm.get('state').value,
            country: this.contactForm.get('country').value,
            zip: this.contactForm.get('postalCode').value,
          },
          contactSocialProfiles: socialProfiles
    
        }
        if(!this.selectedContact){
          this.contactService.addContact(payload).subscribe(res=>{
            console.log(res)
            document.querySelector('.js-spinner-overlay')['style'].display = "none  ";
            this.message.create('success', `Contact added successfully!`);
            this.splitView = false;
            this.split = false;
            setTimeout(() => {
              this.helper.resetFixedState('.js-sticky-index', 'fixed--state');
            }, 50)
            this.getAllContacts();
          },err=>{
            console.log(err)
            document.querySelector('.js-spinner-overlay')['style'].display = "none  ";
            this.message.create('error', `Adding contact failed!`);
          })
        }
        else{
          this.contactService.updateContact(payload, this.selectedContact, this.userInfo.userId).subscribe(res=>{
            console.log(res)
            document.querySelector('.js-spinner-overlay')['style'].display = "none  ";
            this.message.create('success', `Contact updated successfully!`);
            this.splitView = false;
            this.split = false;
            setTimeout(() => {
              this.helper.resetFixedState('.js-sticky-index', 'fixed--state');
            }, 50)
            this.getAllContacts();
          },err=>{
            console.log(err)
            document.querySelector('.js-spinner-overlay')['style'].display = "none  ";
            this.message.create('error', `Update contact failed!`);
          })
        }
        
  }

  selectContact(id = null) {
    console.log(id);
    this.smrtTab2 = 'mine';
    this.splitView = false;
    this.split = false;
    this.resetAll();
    let selectedContact = this.dataSet.find((d) => d.id == id);
    this.selectedContact = id;
    if (selectedContact) {
      selectedContact.selected = !selectedContact.selected;
    }
    let ind = this.selectedContacts.findIndex((contact) => contact?.id == id);
    ind >= 0
      ? this.selectedContacts.splice(ind, 1)
      : this.selectedContacts.push(selectedContact);

      let contact = this.contacts.find(c=>c.id == id);
      this.contactForm.patchValue(contact)
      if(contact.contactAddress){

        this.contactForm.get('country').setValue(contact.contactAddress.country)
        this.contactForm.get('userAddress').setValue(contact.contactAddress.street)
        this.contactForm.get('state').setValue(contact.contactAddress.state)
        this.contactForm.get('postalCode').setValue(contact.contactAddress.zip)
        this.contactForm.get('city').setValue(contact.contactAddress.city)
      }
      if(contact.contactOccupation){

        this.contactForm.get('occupation').setValue(contact.contactOccupation.occupation)
        this.contactForm.get('position').setValue(contact.contactOccupation.position)
        this.contactForm.get('profession').setValue(contact.contactOccupation.profession)
        this.contactForm.get('company').setValue(contact.contactOccupation.companyName)
      }
      console.log(contact)
      contact.contactAttachments.map(att=>{
        this.attachments.push({
        name: att.name,
        attachmentLink: att.attachmentLink,
        type: att.type
        })
      })
      contact.contactSocialProfiles.map(social=>{
        this.socialMedias.push({type: social.type, socialLink: social.socialLink, img: this.socials.find(s=>s.type == social.type).img})
      })
      contact.contactPhoneNumbers.map(ph=>{
        this.phoneNumbers.push({ countryCode: ph.countryCode, number: ph.number, flag: this.codesList.find(x => x.callingCode === ph.countryCode).flag, type: ph.type })
      })
      contact.contactEmails.map(email=>{
        this.emails.push(email.email)
      })
      Object.keys(this.contactForm.controls).map((key, ind) => {
        this.contactForm.controls[key].disable()
      })
    this.splitView = true;
    console.log(this.contactForm.getRawValue(), this.socialMedias)
  }


  fillEmails() {
    return this.emails.map(x => {
      return {
        type: this.profileType,
        email: x,
      }
    })
  }

  checkLetter(letter) {
    return document.getElementById(letter);
  }

  splitScreen(type = null) {
    this.split = true;
    if (type) {
      this.splitView = false;
      this.split = false;
      this.resetAll();
      this.addingContact = true;
      Object.keys(this.contactForm.controls).map((key, ind) => {
        this.contactForm.controls[key].enable()
      })
      this.split = true;
    }
    else {
      this.addingContact = false;
    }
    console.log(this.addingContact)
  }

  sortByThis(type) {
    console.log(this.sorted)
    let tempGrouped = this.sorted.reduce((groups, contact) => {
      const letter = contact[type].charAt(0);

      groups[letter] = groups[letter] || [];
      groups[letter].push(contact);

      var letterCount = this.lettersCount[letter] || 0;
      this.lettersCount[letter] = (letterCount + 1);

      return groups;
    }, {});

    console.log(this.lettersCount);

    this.result = Object.keys(tempGrouped).map((key) => ({
      key,
      contacts: tempGrouped[key],
    }));
    let elems: any = document
      .getElementById('parent-ul')
      ?.getElementsByTagName('li');
    if (elems && elems.length > 0) {
      for (let e of elems) {
        e.classList.remove('active');
      }
    }
    document.getElementById(type)?.classList.add('active');
  }

  noSplit() {
    this.contactForm.reset();
    this.emails = [];
    this.phoneNumbers = [];
    this.socialMedias = [];
    this.attachments = [];
    this.split = false;
    this.splitView = false;
    this.smrtTab2 = 'mine'
    this.updatingContact = false;
    setTimeout(() => {
      this.helper.resetFixedState('.js-sticky-index', 'fixed--state');
    }, 50)
  }

  goToAccount(){
    this.router.navigate(['connect/user-account'])
  }

  showModal(type, id = null) {
    this[type] = !this[type];
    if(id){
      this.selectedContactToPerformAction = id;
    }
  }

  performAction(type){
    if(type == 'showDeleteMsg'){
      this.contactService.deleteContact(this.selectedContactToPerformAction).subscribe(res=>{
        this[type] = !this[type];
        this.message.create('success',res['success'] ?? res['failure'] ?? 'Contact has been deleted')
        this.getAllContacts();
      },err=>{
        this[type] = !this[type];
        this.message.create('error','Failed to delete contact')
      })
    }
    if(type == 'showStarMsg'){
      let payload = {
        id: this.selectedContactToPerformAction,
        userIdWhoFavorited: this.userInfo.userId,
        contactId: this.selectedContactToPerformAction
      }
      this.contactService.addFavouriteContact(payload).subscribe(res=>{
        this[type] = !this[type];
        this.message.create('success',res['success'] ?? res['failure'] ?? 'Contact has been added to favorites')
        this.getAllContacts();
      },err=>{
        this[type] = !this[type];
        this.message.create('error','Failed to add contact to favorites')
      })
    }
    if(type == 'showArchiveMsg'){
      let payload = {
        id: this.selectedContactToPerformAction,
        contactIdWhoArchived: this.userInfo.userId,
        contactId: this.selectedContactToPerformAction
      }
      this.contactService.addArchivedContact(payload).subscribe(res=>{
        this[type] = !this[type];
        this.message.create('success',res['success'] ?? res['failure'] ?? 'Contact has been added to archives')
        this.getAllContacts();
      },err=>{
        this[type] = !this[type];
        this.message.create('error','Failed to add contact to archives')
      })
    }
  }

  ngOnDestroy() {
    console.log('destroyeddd');
  }

  showNotification() {
    this.message.create('info', `No action defined to this button yet!`);
  }

  searchContact(e) {
    console.log('aaaabbbbbbbb');
    let value = e;
    let filter = this.dataSet.filter((data) => data.firstName.includes(value));
    // console.log(e, this.dataSet, filter, this.dataSet.filter(data=>data.firstName.toLowerCase().includes(value)))
    if (
      this.dataSet.filter(
        (data) =>
          data.lastName.toLowerCase().includes(value) ||
          data.firstName.toLowerCase().includes(value)
      ).length >= 0
    ) {
      this.sortData(
        this.dataSet.filter(
          (data) =>
            data.lastName.toLowerCase().includes(value) ||
            data.firstName.toLowerCase().includes(value)
        )
      );
      this.sortByThis('firstName');
    }
  }

  scrollToThis(val) {
    console.log(val);
    console.log(document.getElementById('letter-' + val));
    var objDiv = document.getElementById('letter-' + val);
    objDiv.scrollIntoView({ behavior: 'smooth' });
  }

  allowChanged() {
    console.log('heyy');
    this.allow = !this.allow;
    this.showRoleTemplate = true;
    if (!this.allow) {
      // this.showRoleMsg = true;
    }
    console.log(this.allow);
  }

  searchPermission(e) {
    console.log(e);
    const children = this.nodes[0].children;
    if (e.target.value == '') {
      console.log('here')
      this.searchedNodes = []
      console.log(children)
      this.nodes = [...this.allNodes]
    }
    else {
      let value = e.target.value;
      this.searchedNodes = [...this.nodes];
      this.searchedNodes[0].children = [...this.searchedNodes[0].children.filter(child => child.title.toLowerCase().includes(value))]
      // console.log(this.nodes,this.searchedNodes)
    }
  }

  addAttachment(e) {
    document.getElementById('attachment').click()
  }


  attachmentSelected(e) {
    this.attachments.push({
      img: e.target.files[0],
      imgSrc: URL.createObjectURL(e.target.files[0]),
      name: e.target.files[0].name,
      attachmentLink: URL.createObjectURL(e.target.files[0]),
      type: parseInt(this.profileType),
    })
    let pushedIndex = this.attachments.length - 1;
    let id = this.attachments.length - 1;
    let requestParams = {
      access_token: this.userInfo.accessToken,
      user_id: this.userInfo.userId
    }
    let params = this.helper.prepareRequestParams(requestParams)
    let formData = new FormData();
    formData.append('file',e.target.files[0])

    this.userService.uploadAttachment(params, formData).subscribe((res: any) => {
      console.log("Attachment uploaded", res);
      if (res.fileDownloadUri) {
        this.attachments[pushedIndex].attachmentLink = res.fileDownloadUri;
      }
      this.uploadedAttachments = [...this.attachments]
    })
  }

  pictureChanged(e) {
    if (this.profileType == '1') {
      document.getElementById('indImg').setAttribute('src', URL.createObjectURL(e.target.files[0]))
    }
    else {
      document.getElementById('busImg').setAttribute('src', URL.createObjectURL(e.target.files[0]))
    }
    let requestParams = {
      access_token: this.userInfo.accessToken,
      user_id: this.userInfo.userId
    }
    let params = this.helper.prepareRequestParams(requestParams)
    let formData = new FormData();
    formData.append('file', e.target.files[0])
    this.userService.uploadProfilePicture(params, formData).subscribe((res: any) => {
      console.log('uploaded', res);
      if (res.fileDownloadUri) {
        this.contactForm.controls['profilePicture'].patchValue(res.fileDownloadUri);
      }
    }, err => {
      console.log('err', err)
    })
  }

  nameChanged(type, e) {
    this.contactForm.get(type).setValue(e.target.value)
  }

  valueChanged(e, i) {
    this.emails[i] = e.target.value;
  }
  addEmail() {
    this.emails.push('');
  }

  addPhone() {
    this.phoneNumbers.push({ countryCode: this.codesList[0].callingCode, number: '', flag: this.codesList[0].flag, type: 1 })
  }

  selectFlagByCode(phoneData) {
    let twoDigits = phoneData.number.replace('+', '').substring(0, 2);
    let threeDigits = phoneData.number.replace('+', '').substring(0, 3);

    if (this.codesList.find(x => x.callingCode === threeDigits)) {
      phoneData.flag = this.codesList.find(x => x.callingCode === threeDigits).flag;
    } else if (this.codesList.find(x => x.callingCode === twoDigits)) {
      phoneData.flag = this.codesList.find(x => x.callingCode === twoDigits).flag;
    } else {
      phoneData.flag = this.codesList[0] ? this.codesList[0].flag : "";
    }

    return phoneData.flag;
  }


  addSocial() {
    let socials = JSON.parse(JSON.stringify(this.socials))
    delete socials[0].socialLink
    this.socialMedias.push(socials[0])
    console.log(this.socialMedias)
  }

  socialSelected(index, e) {
    console.log(this.socials);
    let socialMedia: any = this.socials.find(s => s.type == e)
    this.socialMedias[index] = { ...socialMedia };
  }

  removeAttachment(i) {
    this.attachments.splice(i, 1)
    console.log(this.attachments)
  }

  // attachmentSelected(e){
  //   this.attachments.push({
  //     img: e.target.files[0], 
  //     imgSrc: URL.createObjectURL(e.target.files[0]),
  //     name: e.target.files[0].name,
  //     attachmentLink: URL.createObjectURL(e.target.files[0]),
  //     type: 1,
  //   });

  //   let pushedIndex = this.attachments.length - 1;

  //   let requestParams = {
  //     access_token: this.userInfo.accessToken,
  //     user_id: this.userInfo.userId
  //   }
  //   let params = this.helper.prepareRequestParams(requestParams)
  //   let formData = new FormData();
  //   formData.append('file',e.target.files[0])

  //   this.userService.uploadAttachment(params, formData).subscribe((res: any) => {
  //     console.log("Attachment uploaded", res);
  //     if (res.fileDownloadUri) {
  //       this.attachments[pushedIndex].attachmentLink = res.fileDownloadUri;
  //     }
  //   })
  // }

  getAllContacts(){
    document.querySelector('.js-spinner-overlay')['style'].display = "flex";
    this.contactService.getContacts(this.userInfo.userId).subscribe(res=>{
          document.querySelector('.js-spinner-overlay')['style'].display = "none";
      this.contacts = res;
      this.sortData(this.contacts);
    this.sortByThis('firstName');
    },err=>{
          document.querySelector('.js-spinner-overlay')['style'].display = "none";
      console.log(err)
    })
  }

  follow(){
    let payload = {
      description: 'test',
      toUserId: parseInt(this.originals.find(o=>o.contactId == this.selectedContact).id),
      fromUserId: this.userInfo.userId
    }
    this.contactService.followContact(payload).subscribe(res=>{
      console.log(res)
      this.following.push(this.selectedContact)
      this.message.create('success','Follow request has been sent')
    },err=>{
      this.message.create('error','Error sending follow request')
      console.log(err)
    })
  }

  resetAll(){
    this.contactForm.reset();
    this.emails = [];
    this.attachments = [];
    this.socialMedias = [];
    this.phoneNumbers = [];
  }

  openImg(link){
    window.open(link, '_blank')
  }

  getSelectedContactImg(){
    setTimeout(() => {
      return this.contacts.find(c=>c.id == this.selectedContact).profilePicture;
    }, 1000);
  }

  switchTab(tab){
    this.splitView = true;
    this.split = false;
    this.resetAll();
    this.smrtTab2 = tab;
    if(tab == 'original'){
      this.updatingContact = false;
      let requestParams:any = {
        email: []
      }
      this.contacts.find(c=>c.id == this.selectedContact).contactEmails.map(em=>{
        requestParams.email.push(em.email)
      })
      requestParams.email = requestParams.email.join()
      console.log(requestParams)
      let params = this.helper.prepareRequestParams(requestParams)
      console.log(this.originals)
      if(!this.originals.find(o=>o.contactId == this.selectedContact)){
      this.contactService.getOriginalContact(params, this.userInfo.userId).subscribe(res=>{
        if(res){
          this.smrtTab2 = 'original'
        let contact:any = res;
        this.originals.push({...contact, contactId: this.selectedContact})
        this.contactForm.patchValue(contact)
        if(contact.userAddress){
          this.contactForm.get('country').setValue(contact.userAddress.country)
    this.contactForm.get('userAddress').setValue(contact.userAddress.street)
    this.contactForm.get('state').setValue(contact.userAddress.state)
    this.contactForm.get('postalCode').setValue(contact.userAddress.zip)
    this.contactForm.get('city').setValue(contact.userAddress.city)
        }
        if(contact.userOccupation){
          this.contactForm.get('occupation').setValue(contact.userOccupation.occupation)
          this.contactForm.get('position').setValue(contact.userOccupation.position)
          this.contactForm.get('profession').setValue(contact.userOccupation.profession)
          this.contactForm.get('company').setValue(contact.userOccupation.companyName)
        }
    
    
    console.log(contact)
    contact.userAttachments.map(att=>{
      this.attachments.push({
      name: att.name,
      attachmentLink: att.attachmentLink,
      type: att.type
      })
    })
    contact.userSocialProfiles.map(social=>{
      this.socialMedias.push({type: social.type, socialLink: social.socialLink, img: this.socials.find(s=>s.type == social.type).img})
    })
    contact.userPhoneNumbers.map(ph=>{
      this.phoneNumbers.push({ countryCode: ph.countryCode, number: ph.number, flag: this.codesList.find(x => x.callingCode === ph.countryCode).flag, type: ph.type })
    })
    contact.userEmails.map(email=>{
      this.emails.push(email.email)
    })
    Object.keys(this.contactForm.controls).map((key, ind) => {
      this.contactForm.controls[key].disable()
    })
  }
  else{
    this.message.create('error','Contact is not registered on SmartConnect')
    this.smrtTab2 = 'mine'
    this.selectContact(this.selectedContact)
  }
      },err=>{
        console.log(err)
      })
    }
    else{
      let contact = this.originals.find(o=>o.contactId == this.selectedContact);
      this.contactForm.patchValue(contact)
      if(contact.userAddress){
        this.contactForm.get('country').setValue(contact.userAddress.country)
  this.contactForm.get('userAddress').setValue(contact.userAddress.street)
  this.contactForm.get('state').setValue(contact.userAddress.state)
  this.contactForm.get('postalCode').setValue(contact.userAddress.zip)
  this.contactForm.get('city').setValue(contact.userAddress.city)
      }
      if(contact.userOccupation){
        this.contactForm.get('occupation').setValue(contact.userOccupation.occupation)
        this.contactForm.get('position').setValue(contact.userOccupation.position)
        this.contactForm.get('profession').setValue(contact.userOccupation.profession)
        this.contactForm.get('company').setValue(contact.userOccupation.companyName)
      }
    console.log(contact)
    contact.userAttachments.map(att=>{
      this.attachments.push({
      name: att.name,
      attachmentLink: att.attachmentLink,
      type: att.type
      })
    })
    contact.userSocialProfiles.map(social=>{
      this.socialMedias.push({type: social.type, socialLink: social.socialLink, img: this.socials.find(s=>s.type == social.type).img})
    })
    contact.userPhoneNumbers.map(ph=>{
      this.phoneNumbers.push({ countryCode: ph.countryCode, number: ph.number, flag: this.codesList.find(x => x.callingCode === ph.countryCode).flag, type: ph.type })
    })
    contact.userEmails.map(email=>{
      this.emails.push(email.email)
    })
    Object.keys(this.contactForm.controls).map((key, ind) => {
      this.contactForm.controls[key].disable()
    })
    }
    }
    else{
      this.selectContact(this.selectedContact)
    }
  }

  patchContactForm(contact){
    
  }

}
