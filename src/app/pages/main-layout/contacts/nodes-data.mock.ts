export let nodesData = [
  {
    title: 'Smart',
    key: '100',
    expanded: false,
    children: [
      {
        title: 'Connect',
        key: '1001',
        expanded: false,
        children: [
          {
            title: 'Details',
            key: '10010',
            children: [
              {
                title: 'Name',
                key: '100101',
              },
              {
                title: 'Last Name',
                key: '100102',
              },
            ],
          },
        ],
      },
      {
        title: 'Pointer',
        key: '1002',
      },
      {
        title: 'Signup',
        key: '1002',
      },
    ],
    
  },
];