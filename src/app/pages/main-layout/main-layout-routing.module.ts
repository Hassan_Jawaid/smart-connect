import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactsComponent } from './contacts/contacts.component';
import { ContactsConnectedComponent } from './connected/connected.component';
import { InformationTabComponent } from './privacy/information-tab/information-tab.component';
import { PrivacyTabComponent } from './privacy/privacy-tab/privacy-tab.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { RolesTabComponent } from './privacy/roles-tab/roles-tab.component';
import { SessionsComponent } from './sessions/sessions.component';
import { SuggestionsComponent } from './suggestions/suggestions.component';
import { FavouritesComponent } from './favourites/favourites.component';
import { ArchivesComponent } from './archives/archives.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { PendingSessionComponent } from './pending-sessions/pending-sessions.component';

const routes: Routes = [
  { path: 'user-account', component: UserAccountComponent },
  { path: 'sessions', component: SessionsComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: '', component: ContactsComponent },
  
  { path: 'connected', component: ContactsConnectedComponent },
  { path: 'favourites', component: FavouritesComponent },
  { path: 'archives', component: ArchivesComponent },
  { path: 'privacy', component: PrivacyComponent },
  { path: 'pending-sessions', component: PendingSessionComponent },
  { path: 'privacy/privacy-tab', component: PrivacyTabComponent },
  { path: 'privacy/roles-tab', component: RolesTabComponent },
  { path: 'privacy/information-tab', component: InformationTabComponent },
  { path: 'suggestions', component: SuggestionsComponent },
  { path: '**', redirectTo: '/connect/contacts' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainLayoutRoutingModule { }
