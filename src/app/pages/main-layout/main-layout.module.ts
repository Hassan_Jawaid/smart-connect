import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainLayoutRoutingModule } from './main-layout-routing.module';
// import { DashboardComponent } from './dashboard/dashboard.component';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserAccountComponent } from './user-account/user-account.component';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { SessionsComponent } from './sessions/sessions.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { ConfirmationModalComponent } from 'src/app/shared/layouts/confirmation-modal/confirmation-modal.component';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { ContactsComponent } from './contacts/contacts.component';
import { ContactsConnectedComponent } from './connected/connected.component';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { PrivacyComponent } from './privacy/privacy.component';
import { SuggestionsComponent } from './suggestions/suggestions.component';
import { PrivacyTabComponent } from './privacy/privacy-tab/privacy-tab.component';
import { RolesTabComponent } from './privacy/roles-tab/roles-tab.component';
import { InformationTabComponent } from './privacy/information-tab/information-tab.component';
import { FavouritesComponent } from './favourites/favourites.component';
import { ArchivesComponent } from './archives/archives.component';
import { PendingSessionComponent } from './pending-sessions/pending-sessions.component';
import { SessionSplitViewComponent } from '../../shared/layouts/session-split-view/session-split-view.component';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';


@NgModule({
  declarations: [UserAccountComponent, SessionsComponent, PendingSessionComponent, ArchivesComponent, FavouritesComponent, ContactsConnectedComponent, ContactsComponent, PrivacyComponent, SuggestionsComponent, PrivacyTabComponent, RolesTabComponent, InformationTabComponent, SessionSplitViewComponent,],
  imports: [
    CommonModule,
    MainLayoutRoutingModule,
    NzLayoutModule,
    NzCardModule,
    SharedModule,
    NzRadioModule,
    NzGridModule,
    NzButtonModule,
    NzTableModule,
    NzCheckboxModule,
    NzMessageModule,
    NzTabsModule,
    NzDatePickerModule,
  ],
  exports: [
    NzLayoutModule,
    NzCardModule,
    NzRadioModule,
    NzGridModule,
    NzButtonModule,
    NzTableModule,
    NzCheckboxModule,
    NzTabsModule,
  ]
})
export class MainLayoutModule { }
