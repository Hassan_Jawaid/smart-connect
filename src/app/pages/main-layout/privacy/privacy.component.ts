import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.less']
})
export class PrivacyComponent implements OnInit {

  constructor(private shared: SharedService, private router: Router) {
    router.navigate(['connect/privacy/privacy-tab'])
  }
  
  ngOnInit(): void {
  }


}
