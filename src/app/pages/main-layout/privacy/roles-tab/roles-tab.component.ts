import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-roles-tab',
  templateUrl: './roles-tab.component.html',
  styleUrls: ['./roles-tab.component.less']
})
export class RolesTabComponent implements OnInit {

  constructor(private message: NzMessageService) { }
  roles = [
    'Sale','Receivable Accounts','User','Counter','Administrator'
  ];
  allRoles = this.roles;
  nodes = [
    {
      title: 'Smart',
      key: '100',
      expanded: false,
      children: [
        {
          title: 'Connect',
          key: '1001',
          expanded: false,
          children: [
            {
              title: 'Details',
              key: '10010',
              children: [
                {
                  title: 'Name',
                  key: '100101',
                },
                {
                  title: 'Last Name',
                  key: '100102',
                },
              ],
            },
          ],
        },
        {
          title: 'Pointer',
          key: '1002',
        },
        {
          title: 'Signup',
          key: '1002',
        },
      ],
      
    },
  ];
  searchValue;
  deleteModalProps = {
    title: 'Are you sure you want to delete this role?',
    primaryBtn : 'Yes',
    secondaryBtn: 'No'
  }

  copyModalProps = {
    title: 'Only allowed data will be duplicated',
    primaryBtn : 'Ok',
    secondaryBtn: null
  }

  deletePermissionModalProps = {
    title: 'Are you sure you want to delete this permission?',
    primaryBtn : 'Yes',
    secondaryBtn: 'No'
  }

  title;
  primaryBtn;
  secondaryBtn;
  showModal = false;

  ngOnInit(): void {
  }

  showNotification(){
    this.message.create('info', `No action defined to this button yet!`);
  }

  toggleModal(modal){
    if(modal){
      this.showModal = true;
      this.title = modal == 'delete' ? 
      this.deleteModalProps.title : 'deletePermission' ? this.deletePermissionModalProps.title : this.copyModalProps.title;
      this.primaryBtn = modal == 'delete' ? 
      this.deleteModalProps.primaryBtn : 'deletePermission' ? this.deletePermissionModalProps.primaryBtn : this.copyModalProps.primaryBtn;
      this.secondaryBtn = modal == 'delete' ? 
      this.deleteModalProps.secondaryBtn : 'deletePermission' ? this.deletePermissionModalProps.secondaryBtn : this.copyModalProps.secondaryBtn;
    }
    else{
      this.showModal = false;
    }
  }
  sortByThis(type){
   let elems:any = document.getElementById('parent-ul')?.getElementsByTagName('li');
   if(elems && elems.length>0){ for(let e of elems){
    e.classList.remove('active');
   }}
   document.getElementById(type)?.classList.add('active')
  }

  searchRole(e){
    if(e == ''){
      this.roles = this.allRoles;
    }
    else{
      let value = e;
      let filter = this.roles.filter(data=>data.toLowerCase().includes(value));
      this.roles = filter;
    }
    
  }

}
