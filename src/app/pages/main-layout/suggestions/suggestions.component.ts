import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-suggestions',
  templateUrl: './suggestions.component.html',
  styleUrls: ['./suggestions.component.less']
})
export class SuggestionsComponent implements OnInit {

  constructor(private message: NzMessageService) { }
  split = false;
  showFollowMsg;
  profileType = '1';
  showAcceptMsg;
  selectedContacts = [];
  showDeleteMsg;
  emails = ['test@test.com'];
  dataSet = [
    {
      firstName: 'Andrew',
      lastName: 'Raymond',
      email: 'abdul@test.com',
      country: 'Italy',
      phone: '03213124431',
      selected: false,
      id: 9,
          picture: 'https://www.microsoft.com/en-us/research/wp-content/uploads/2017/09/avatar_user_36443_1506533427.jpg'
  
    },
    {
      firstName: 'Abey',
      lastName: 'Lint',
      email: 'abid@test.com',
      phone: '03213124431',
      country: 'Australia',
      selected: false,
      id: 3,
          picture: 'https://media-exp1.licdn.com/dms/image/C4D03AQFS5J1YSzJE4Q/profile-displayphoto-shrink_200_200/0/1574502943302?e=1624492800&v=beta&t=hwYBfOSpOAQtCZ6YRFFcd8QJqXnxtff_QAEbETIv3uY'
  
    },
    {
    firstName: 'Ashley',
    lastName: 'Carl',
    email: 'ali@test.com',
    phone: '03213124431',
    country: 'Spain',
    selected: false,
    id: 1,
    picture: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80'
  },
  
  {
    firstName: 'Brandon',
    lastName: 'Jones',
    email: 'bilal@test.com',
    phone: '03213124431',
    country: 'Russia',
    selected: false,
    id: 2,
        picture: 'https://portotheme.com/html/porto/7.5.0/img/avatars/avatar.jpg'

  },
  
  {
    firstName: 'Charlie',
    lastName: 'Young',
    email: 'chaudhry@test.com',
    phone: '03213124431',
    country: 'Germany',
    selected: false,
    id: 4,
        picture: 'https://images-na.ssl-images-amazon.com/images/I/81K7JEQaXjL._SY600_.jpg'

  },
  {
    firstName: 'Danny',
    lastName: 'Hart',
    email: 'dani@test.com',
    phone: '03213124431',
    country: 'Australia',
    selected: false,
    id: 5,
        picture: 'http://demos.thementic.com/wordpress/WC01/WC010007/wp-content/uploads/2019/02/t3.jpg'

  },
  {
    firstName: 'Douglas',
    lastName: 'Costa',
    email: 'fhahroz@test.com',
    phone: '03213124431',
    country: 'England',
    selected: false,
    id: 8,
        picture: 'https://amzsummits.com/wp-content/uploads/2019/05/Ferry-Vermeulen.jpeg'

  },
  {
    firstName: 'George',
    lastName: 'Sandow',
    email: 'hamza@test.com',
    phone: '03213124431',
    country: 'Zimbabwe',
    selected: false,
    id: 10,
        picture: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80'

  },
  {
    firstName: 'Shawn',
    lastName: 'Michaels',
    email: 'haseeb@test.com',
    phone: '03213124431',
    country: 'England',
    selected: false,
    id: 7,
        picture: 'https://www.niemanlab.org/images/Greg-Emerson-edit-2.jpg'

  },
  {
    firstName: 'Tom',
    lastName: 'Garay',
    email: 'sherry@test.com',
    phone: '03213124431',
    country: 'South Africa',
    selected: false,
    id: 6,
        picture: 'https://cxl.com/wp-content/uploads/2016/03/nate_munger.png'

  }
  ]
  
  sorted = this.dataSet.sort((a, b) => a.firstName > b.firstName ? 1 : -1);

grouped = this.sorted.reduce((groups, contact) => {
    const letter = contact.firstName.charAt(0);

    groups[letter] = groups[letter] || [];
    groups[letter].push(contact);

    return groups;
}, {});

 result = Object.keys(this.grouped).map(key => ({key, contacts: this.grouped[key]}));


  ngOnInit(): void {
  }

  sortData(data){
    this.sorted = data.sort((a, b) => a.firstName > b.firstName ? 1 : -1);
  }

  selectAll() {
    this.result.forEach(x => {
      x.contacts.forEach(y => {
        y['selected'] = true;
      });
    })
  }
  
  sortByThis(type){
    let tempGrouped = this.sorted.reduce((groups, contact) => {
      const letter = contact[type].charAt(0);
  
      groups[letter] = groups[letter] || [];
      groups[letter].push(contact);
  
      return groups;
  }, {});
  
   this.result = Object.keys(tempGrouped).map(key => ({key, contacts: tempGrouped[key]}));
   let elems:any = document.getElementById('parent-ul')?.getElementsByTagName('li');
   if(elems && elems.length>0){ for(let e of elems){
    e.classList.remove('active');
   }}
   document.getElementById(type)?.classList.add('active')
  }

  showModal(type) {
    this[type] = !this[type];
  }

  viewProfile(){
    this.split = !this.split;
  }

  addEmail(){
    this.emails.push('')
  }

  showNotification() {
    this.message.create('info', `No action defined to this button yet!`);
  }

  searchContact(e){
    console.log('aaaabbbbbbbb')
    let value = e;
    let filter = this.dataSet.filter(data=>data.firstName.includes(value));
    console.log(filter)
    // console.log(e, this.dataSet, filter, this.dataSet.filter(data=>data.firstName.toLowerCase().includes(value)))
    if(this.dataSet.filter(data=>
      data.lastName.toLowerCase().includes(value) || data.firstName.toLowerCase().includes(value)
      ).length >= 0){
        console.log('sorting')
      this.sortData(this.dataSet.filter(data=>data.lastName.toLowerCase().includes(value) || data.firstName.toLowerCase().includes(value)))
      this.sortByThis('firstName');
    }
  }

  selectContact(id = null){
    console.log(id)
    let selectedContact = this.dataSet.find(d=>d.id == id);
    if(selectedContact){selectedContact.selected = !selectedContact.selected}
    let ind = this.selectedContacts.findIndex(contact=>contact.id == id)
    ind >= 0 ? this.selectedContacts.splice(ind, 1) : this.selectedContacts.push(selectedContact)
  }

}
