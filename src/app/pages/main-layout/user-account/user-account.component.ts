import { identifierModuleUrl } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { UserService } from 'src/app/services/user.service';
import { SharedService } from 'src/app/shared/shared.service';
import { AppHelper } from 'src/app/shared/utils/helper';
import { AuthService } from '../../auth/auth.service';


@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.less']
})
export class UserAccountComponent implements OnInit {
  @Input() inherited: any

  constructor(private router: Router, private sharedService: SharedService, private helper: AppHelper, private authService: AuthService, private userService: UserService) { }
  emails:any = [{email: '', type: 1}];
  attachments = [];
  phoneNumbers:any = [];
  socials = [
    {type: 1, name: 'Facebook', img: 'https://raw.githubusercontent.com/edent/SuperTinyIcons/master/images/svg/facebook.svg'},
    {type: 2, name: 'Instagram', img: 'https://raw.githubusercontent.com/edent/SuperTinyIcons/master/images/svg/instagram.svg'},
    {type: 3, name: 'Snapchat',img: 'https://raw.githubusercontent.com/edent/SuperTinyIcons/master/images/svg/snapchat.svg'},
    {type: 4, name: 'Twitter', img: 'https://raw.githubusercontent.com/edent/SuperTinyIcons/master/images/svg/twitter.svg'}
  ]
  socialMedias:any = [];
  userPayload;
  userForm = new FormGroup({
    userType: new FormControl('1', Validators.required),
    firstName: new FormControl({value: '', disabled: true}),
    lastName: new FormControl({value: '', disabled: true}),
    birthday: new FormControl(null),
    gender: new FormControl({value: '', disabled: true}),
    country: new FormControl({value: '', disabled: true}),
    state: new FormControl({value: '', disabled: true}),
    city: new FormControl({value: '', disabled: true}),
    postalCode: new FormControl({value: '', disabled: true}),
    userAddress: new FormControl({value: '', disabled: true}),
    profession: new FormControl({value: '', disabled: true}),
    occupation: new FormControl({value: '', disabled: true}),
    company: new FormControl({value: '', disabled: true}),
    position: new FormControl({value: '', disabled: true}),
    website: new FormControl({value: '', disabled: true}),
    registrationEmail: new FormControl({value: '', disabled: true}),
    notes: new FormControl({value: '', disabled: true}),
    profilePicture: new FormControl({value: ''}),
  })

  img1 = false;
  img2 = false;
  profileType = '1';
  date;
  individualEdit = false;
  businessEdit = false;
  showDeleteMsg = false;
  showStarMsg = false;
  showDeleteImg = false;
  countriesList:any = [];
  mode = 'view';
  userInfo = this.authService.getToken();
  codesList:any = [];
  showCodesDropdown = false;

  ngOnInit(): void {
    console.log(this.inherited)
    this.sharedService.getAllCountries().subscribe(res => {
      this.countriesList = res;
    });
    this.sharedService.getAllCodes().subscribe(res=>{
      this.codesList = res;
      this.codesList.map(c=>{
        c.callingCode = c.callingCodes[0];
        delete c.callingCodes;
      })
    })
    // (this.userForm.get('userEmails') as FormArray).push(new FormControl('', Validators.required));
    // (this.userForm.get('userPhoneNumbers') as FormArray).push(new FormControl('', Validators.required));
    console.log(this.userForm.getRawValue())
    this.getUser();
    console.log(this.socials);
  }

  typeChanged(){
    console.log('changed')
    this.businessEdit = false
    this.individualEdit = false
  }

  image1Selected(){
    this.img1 = true;
    this.img2 = false;
    // document.getElementById('minus-1')['style'].top = '14px';
    document.getElementById('img-1').classList.add('selected');
    document.getElementById('img-2').classList.remove('selected');
  }

  image2Selected(){
    this.img2 = true;
    this.img1 = false;
    // document.getElementById('minus-2')['style'].top = '28px';
    document.getElementById('img-2').classList.add('selected');
    document.getElementById('img-1').classList.remove('selected');
  }

  startEdit(){
    console.log(this.userForm.controls)
    Object.keys(this.userForm.controls).map((key, ind) => {
      this.userForm.controls[key].enable()
    })
    this.individualEdit = this.profileType == '1' ? true : false
    this.businessEdit = this.profileType == '2' ? true : false
    this.mode = 'edit'
  }

  accept(type){
    let phones = [...this.phoneNumbers];
    phones.forEach(ph=>{
      delete ph.flag;
    });
    let socialProfiles = [...this.socialMedias];
    console.log(this.userForm.getRawValue())
    let payload = {...this.userPayload}
    payload.firstName = this.userForm.get('firstName').value,
    payload.lastName = this.userForm.get('lastName').value,
    payload.birthday = this.userForm.get('birthday').value,
    payload.userOccupation = {
      companyName: this.userForm.get('company').value,
      position: this.userForm.get('position').value,
      occupation: this.userForm.get('occupation').value
    }
    payload.userAddress = {
      street: this.userForm.get('userAddress').value,
      city: this.userForm.get('city').value,
      state: this.userForm.get('state').value,
      country: this.userForm.get('country').value,
      zip: this.userForm.get('postalCode').value,
      
    }
    payload.userAttachments = this.attachments;
    payload.userEmails = this.fillEmails(),
    payload.userPhoneNumbers = phones,
    payload.userSocialProfiles = socialProfiles
    payload.profilePicture = this.userForm.get('profilePicture')?.value ? this.userForm.get('profilePicture').value : '';
    document.querySelector('.js-spinner-overlay')['style'].display = "flex";
    this.userService.updateUser(payload, this.userInfo.userId).subscribe(res=>{
      document.querySelector('.js-spinner-overlay')['style'].display = "none";
      this[type] = false;
      Object.keys(this.userForm.controls).map((key, ind) => {
        this.userForm.controls[key].disable()
      })
      console.log(res)
    },err=>{
      document.querySelector('.js-spinner-overlay')['style'].display = "none";
      console.log(err)
    })
  }

  selectFlagByCode(phoneData) {
    let twoDigits = phoneData.number.replace('+', '').substring(0,2);
    let threeDigits = phoneData.number.replace('+', '').substring(0,3);
    
    if (this.codesList.find(x => x.callingCode === threeDigits)) {
      phoneData.flag = this.codesList.find(x => x.callingCode === threeDigits).flag;
    } else if (this.codesList.find(x => x.callingCode === twoDigits)) {
      phoneData.flag = this.codesList.find(x => x.callingCode === twoDigits).flag;
    } else {
      phoneData.flag = this.codesList[0] ? this.codesList[0].flag : "";
    }
    
    return phoneData.flag;
  }

  fillEmails() {
    return this.emails.map(x => {
      return {
        type: parseInt(this.profileType),
        email: x,
      }
    })
  }

  showModal(type){
    this[type] = !this[type];
  }

  openImg(link){
    window.open(link);
  }
  pictureChanged(e){
    if(this.profileType == '1'){
      document.getElementById('indImg').setAttribute('src', URL.createObjectURL(e.target.files[0]))
    }
    else{
      document.getElementById('busImg').setAttribute('src', URL.createObjectURL(e.target.files[0]))

    }
    let requestParams = {
      access_token: this.userInfo.accessToken,
      user_id: this.userInfo.userId
    }
    let params = this.helper.prepareRequestParams(requestParams)
    let formData = new FormData();
    formData.append('file',e.target.files[0])
    this.userService.uploadProfilePicture(params, formData).subscribe((res: any)=>{
      console.log('uploaded',res);
      if (res.fileDownloadUri) {
        this.userForm.controls['profilePicture'].patchValue(res.fileDownloadUri);
      }
    },err=>{
      console.log('err',err)
    })
  }

  addEmail(){
    this.emails.push('');
    // (this.userForm.get('userEmails') as FormArray).push(new FormControl('', Validators.required))
    console.log(this.userForm.getRawValue())
  }

  addPhone(){
    this.phoneNumbers.push({countryCode: this.codesList[0].callingCode, number: '', flag: this.codesList[0].flag, type: 1})
    console.log(this.userForm.getRawValue())
  }

  addSocial(){
    this.socialMedias.push(this.socials[0])
    console.log(this.userForm.getRawValue())
  }

  getUser(){
    document.querySelector('.js-spinner-overlay')['style'].display = "flex";
    this.userService.getUser(this.userInfo.userId).subscribe((res:any)=>{
      document.querySelector('.js-spinner-overlay')['style'].display = "none";
      res.city = res.userAddress?.city;
      res.postalCode = res.userAddress?.zip;
      res.state = res.userAddress?.state;
      res.country = res.userAddress?.country;
      res.userAddress = res.userAddress?.street;
      res.company = res.userOccupation?.companyName;
      res.position = res.userOccupation?.position;
      res.occupation = res.userOccupation?.occupation;
      this.userForm.patchValue(res)
      this.userPayload = res;
      if(res['birthday'] == '-'){
        this.userForm.get('birthday').setValue('')
      }
      if(res['userEmails'].length){
        this.emails = [];
        res['userEmails'].map(em=>{
          this.emails.push(em.email)
        })
        console.log(this.emails)
      }
      if(res['userAttachments'].length){
        res['userAttachments'].map(att=>{
          this.attachments.push(att)
        })
      }
      if(res['userPhoneNumbers'].length){
        res['userPhoneNumbers'].map(ph=>{
          this.phoneNumbers.push({countryCode: ph.countryCode, number: ph.number, type: ph.type})
        })
      }
      if(res['userSocialProfiles'].length){
        res['userSocialProfiles'].map(soc => {
          var image = this.socials.find(x => x.type === soc.type).img;
          this.socialMedias.push({...soc, img: image});
        })
      }
    },err=>{
      console.log(err)
    })
  }

  valueChanged(e,i){
    this.emails[i] = e.target.value;
  }


  cancel(type){
    Object.keys(this.userForm.controls).map((key, ind) => {
      this.userForm.controls[key].disable()
    })
    this[type] = false;
  }

  codeSelected(e,i){
    let country = this.codesList.find(c=>c.callingCode == e)
    this.phoneNumbers[i].flag = country.flag;
  }

  socialSelected(index, e){
    console.log(this.socials);
    let socialMedia:any = this.socials.find(s=>s.type == e)
    this.socialMedias[index] = {...socialMedia};
  }

  addAttachment(){
    document.getElementById('attachment').click()
  }

  attachmentSelected(e){
    this.attachments.push({
      img: e.target.files[0], 
      imgSrc: URL.createObjectURL(e.target.files[0]),
      name: e.target.files[0].name,
      attachmentLink: URL.createObjectURL(e.target.files[0]),
      type: 1,
    });

    let pushedIndex = this.attachments.length - 1;

    let requestParams = {
      access_token: this.userInfo.accessToken,
      user_id: this.userInfo.userId
    }
    let params = this.helper.prepareRequestParams(requestParams)
    let formData = new FormData();
    formData.append('file',e.target.files[0])

    this.userService.uploadAttachment(params, formData).subscribe((res: any) => {
      console.log("Attachment uploaded", res);
      if (res.fileDownloadUri) {
        this.attachments[pushedIndex].attachmentLink = res.fileDownloadUri;
      }
    })
  }

  performAction(scope: string) {
    switch (scope) {
      case 'value':
        
        break;
    }
  }

}
