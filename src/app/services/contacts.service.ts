import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AuthService } from '../pages/auth/auth.service';
import { ApiService } from '../shared/api.service';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  
  userInfo = this.authService.getToken();
  constructor(private authService: AuthService, private api: ApiService) { }
  
  getContacts(id) {
    let headers = {access_token: this.userInfo.accessToken}
    return this.api.get('/contact/getContactsForUser/'+id, null, headers)
  }

  addContact(body) {
    let headers = {access_token: this.userInfo.accessToken}
    return this.api.post('/contact/createContact', body, null, headers)
  }

  updateContact(body, id, userid) {
    let headers = {access_token: this.userInfo.accessToken, logged_in_user_id: userid}
    return this.api.put('/contact/updateContact/'+id, body, null, headers)
  }

  followContact(body) {
    let headers = {access_token: this.userInfo.accessToken, "Accept": "application/json" }
    
    return this.api.post('/follower/sendFollowRequest', body, null, headers)
  }

  deleteContact(id) {
    let headers = {access_token: this.userInfo.accessToken}
    return this.api.delete('/contact/deleteContact/'+id, headers)
  }

  addFavouriteContact(body) {
    let headers = {access_token: this.userInfo.accessToken }
    return this.api.post('/favoriteContact/addFavoriteContact', body, null, headers)
  }

  addArchivedContact(body) {
    let headers = {access_token: this.userInfo.accessToken }
    return this.api.post('/archiveContact/addArchiveContact', body, null, headers)
  }

  getOriginalContact(params, id){
    let headers = {access_token: this.userInfo.accessToken, logged_in_user_id: id}
    return this.api.get('/user/getOriginalUserForContactMappedId', params, headers)
    
  }

}
