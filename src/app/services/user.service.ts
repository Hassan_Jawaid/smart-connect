import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from '../pages/auth/auth.service';
import { ApiService } from '../shared/api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userInfo = this.authService.getToken();
  constructor(private authService: AuthService, private api: ApiService) { }
  
  getUser(id) {
    let headers = {access_token: this.userInfo.accessToken, logged_in_user_id: id}
    return this.api.get('/user/getUser/'+id, null, headers)
  }

  updateUser(body, id) {
    let headers = {access_token: this.userInfo.accessToken, logged_in_user_id: id}
    return this.api.put('/user/updateUser/'+id, body, null, headers)
  }

  uploadProfilePicture(params, body){
      const headers = new HttpHeaders({
       "Accept": "*/*" 
      })
    
    return this.api.post('/uploadProfilePicture', body, params, headers)
  }

  uploadAttachment(params, body){
      const headers = new HttpHeaders({
       "Accept": "*/*" 
      })
    
    return this.api.post('/uploadAttachment', body, params, headers)
  }
}
