import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  get(url: string, params: HttpParams = new HttpParams(), headers = {}){
    let fullURL = `${environment.baseUrl}${url}`;
    if (url.includes('http')) {
      fullURL = url;
    }
    return this.http.get(`${fullURL}`, {params, headers}).pipe(
        map(res => res)
      );
  }

  put(url, body = {}, params: HttpParams = new HttpParams(), headers = {}){
    return this.http.put(`${environment.baseUrl}${url}`, body, {params, headers}).pipe(
        map(res => res)
      );
  }

  post(url, body = {}, params: HttpParams = new HttpParams(), headers = {}){
    return this.http.post(`${environment.baseUrl}${url}`, body, {params, headers}).pipe(
        map(res => res)
      );
  }

  delete(url, headers = {}){
    return this.http.delete(`${environment.baseUrl}${url}`,{headers}).pipe(
        map(res => res)
      );
  }
}
