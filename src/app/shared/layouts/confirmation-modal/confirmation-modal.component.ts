import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.less']
})
export class ConfirmationModalComponent implements OnInit {
  @Input() title: any;
  @Input() button1: any;
  @Input() button2: any;
  @Input() button3: any;
  
  @Output() cancel = new EventEmitter()
  @Output() yes = new EventEmitter()

  constructor() { }

  ngOnInit(): void {
    console.log('helloooooooo')
  }

  cancelModal(){
    this.cancel.emit(null)
  }

  success(){
    console.log('asd')
    this.yes.emit(null)
  }

}
