import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/pages/auth/auth.service';
import { SharedService } from '../../shared.service';
@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.less']
})
export class MainLayoutComponent implements OnInit {
  isCollapsed = true;
  isCollapsedUser = true;
  showMsg = false;
  url;
  submitted = false;
  showLogoutMsg = false;
  showArchiveMsg = false;
  accessExpanded = false;
  preferenceExpanded = false;
  tab = ''
  userInfo = this.auth.getToken();

  constructor(private router: Router, private route: ActivatedRoute, private auth: AuthService, private shared: SharedService) { }

  ngOnInit() {
    this.url = this.route.snapshot['_routerState'].url;
    console.log(this.url)
    if(this.url.includes('privacy')){
      console.log('hhhh')
      this.expandAccess();
    }
  }

  changePassword(){
    this.router.navigate(['change-password']);
  }

  deactivate(){
    this.showMsg = true;
  }

  cancelled(){
    this.showMsg = false;
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['auth/login'])
  }

  collapse(){
    console.log('heree')
    this.isCollapsed = !this.isCollapsed;
  }

  showModal(type){
    this[type] = !this[type];
  }

  goToPage(route){
    this.router.navigate(['connect/'+route]) 
  }

  goToPrivacy(){
    this.router.navigate(['privacy']) 
  }

  goToAccount(){
    this.router.navigate(['connect/user-account'])
  }

  expandAccess(){
    this.setTab('privacy')
    if(!this.isCollapsed){
      this.accessExpanded = !this.accessExpanded;
      this.preferenceExpanded = false;
    }
    
  }
  expandPreference(){
    this.setTab('information')
    if(!this.isCollapsed){
      this.preferenceExpanded = !this.preferenceExpanded;
      this.accessExpanded = false;
    }
    
  }

  setTab(name){
    this.shared.setTabName(name)
  }

  routeToTab(name){
    console.log('in route to tab')
    this.router.navigate(['connect/privacy/'+name+'-tab'])
    this.shared.setTabName(name)
    this.tab = name;
  }

  DeactivateAccount(){
    this.submitted = true;
  }

  goToContacts(){
    this.router.navigate(['connect/contacts'])
  }

}
