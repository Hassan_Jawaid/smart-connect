import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'session-split-view',
  templateUrl: './session-split-view.component.html',
  styleUrls: ['./session-split-view.component.less']
})
export class SessionSplitViewComponent implements OnInit {
  profileType = '1';
  showDeleteImg = false;
  
  constructor() { }

  ngOnInit() {
  }

  
  showModal(type){
    this[type] = !this[type];
  }

}
