import { NgModule } from '@angular/core';

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { IconsProviderModule } from '../icons-provider.module';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { CommonModule, registerLocaleData } from '@angular/common';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import en from '@angular/common/locales/en';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCardModule } from 'ng-zorro-antd/card';
import { UserSidebarComponent } from './layouts/user-sidebar/user-sidebar.component';
import { ConfirmationModalComponent } from './layouts/confirmation-modal/confirmation-modal.component';
// import { SidebarComponent } from './layouts/sidebar/sidebar.component';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzTreeModule } from 'ng-zorro-antd/tree';
import { NotFoundComponent } from './layouts/not-found/not-found.component';
import { PrivacySidebarComponent } from './layouts/privacy-sidebar/privacy-sidebar.component';
import { LoadSvg } from './utils/component/load-svg/load-svg.component';
import { NoAccessComponent } from './layouts/no-access/no-access.component';
import { NzBadgeModule } from 'ng-zorro-antd/badge';


registerLocaleData(en);

@NgModule({
  imports: [
    CommonModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    FormsModule,
    ReactiveFormsModule,
    NzInputModule,
    NzFormModule,
    NzSwitchModule,
    NzRadioModule,
    NzGridModule,
    NzDropDownModule,
    NzModalModule,
    NzCardModule,
    NzSelectModule,
    NzCheckboxModule,
    NzTreeModule,
    NzBadgeModule
  ],
  exports: [
    CommonModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    FormsModule,
    ReactiveFormsModule,
    NzInputModule,
    NzFormModule,
    NzSwitchModule,
    NzRadioModule,
    NzGridModule,
    NzDropDownModule,
    NzModalModule,
    NzCardModule,
    NzSelectModule,
    ConfirmationModalComponent,
    NzCheckboxModule,
    NzTreeModule,
    LoadSvg,
    NzBadgeModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  declarations: [ConfirmationModalComponent, NotFoundComponent, PrivacySidebarComponent, LoadSvg, NoAccessComponent],
  // declarations: [SidebarComponent],
  // declarations: [UserSidebarComponent],
})
export class SharedModule { }
