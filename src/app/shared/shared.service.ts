import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private api: ApiService) { }

  private tabName = new BehaviorSubject('privacy');

  setTabName(tabname){
    console.log(tabname)
    this.tabName.next(tabname)
  }

  getTabName(): Observable<any>{
    return this.tabName.asObservable()
  }

  getAllCountries() {
    return this.api.get('https://restcountries.eu/rest/v2/all?fields=name;alpha3Code;flag');
  }

  getAllCodes(){
    return this.api.get('https://restcountries.eu/rest/v2/all?fields=callingCodes;flag');
  }



}
