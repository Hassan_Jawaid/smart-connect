import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'load-svg',
  templateUrl: './load-svg.component.html',
  styleUrls: ['./load-svg.component.less']
})
export class LoadSvg implements OnInit {
  htmlString: any = '';
  @Input('url') url: string;

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {

  }

  ngOnInit(): void {
    this.http.get(this.url, {responseType: 'text'}).subscribe((res) => {
      this.htmlString = res.toString();
      this.htmlString = this.sanitizer.bypassSecurityTrustHtml(this.htmlString);
      // console.log('============', this.htmlString);
    })
  }

}
