import { HttpParams } from "@angular/common/http";

export class AppHelper {

    prepareRequestParams(body){
        let params = new HttpParams()
        for(let single in body){
            params = params.append(single, body[single]);
        }
        return params;
    }

    intiateFixedState(element: Element, className: string) {
        let doc = document.documentElement;
        let top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
        let elementLeftPosition = 0;
        let elementTopPosition = 0;
        
        // element.classList.remove(className);
        if (element.getAttribute('data-affix-left')) {
            elementLeftPosition = parseFloat(element.getAttribute('data-affix-left'));
        } else {
            elementLeftPosition = this.offset(element).left;
            element.setAttribute('data-affix-left', elementLeftPosition.toString());
        }
        
        if (element.getAttribute('data-affix-top')) {
            elementTopPosition = parseFloat(element.getAttribute('data-affix-top'));
        } else {
            elementTopPosition = this.offset(element).top;
            element.setAttribute('data-affix-top', elementTopPosition.toString());
        }
        
        console.log("Here", top, this.offset(element).top);

        if (top >= elementTopPosition) {
            element.classList.add(className);
            element['style'].left = `${elementLeftPosition}px`;
            element['style'].right = `initial`;
        } else {
            element.classList.remove(className);
            element['style'].left = `initial`;
            element['style'].right = `6px`;
        }
    }

    resetFixedState(selector: string, className: string) {
        document.querySelectorAll(selector).forEach((element) => {
            element.classList.remove(className);
            element['style'].left = `initial`;
            element['style'].right = `6px`;
            element.setAttribute('data-affix-left', this.offset(element).left);
            element.setAttribute('data-affix-top', this.offset(element).top);
        });
    }

    initSticky(selector: string, className: string) {

        this.resetFixedState(selector, className);
        
        window.addEventListener('scroll', () => {
            document.querySelectorAll(selector).forEach((element) => {
                this.intiateFixedState(element, className);
            });
        });
    }

    offset(el) {
        var rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
    }

}
